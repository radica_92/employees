<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use app\models\User;
use yii\base\Model;
use app\models\DailyScrum;


/**
 * This is the model class for table "participation".
 *
 * @property int $daily_scrum_id
 * @property int $user_id
 * @property string $time_of_arrival
 *
 * @property DailyScrum $dailyScrum
 * @property User $user
 */
class Participation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'time_of_arrival'], 'required'],
            [['daily_scrum_id', 'user_id'], 'integer'],
            [['time_of_arrival'], 'safe'],
			[['time_of_arrival'], 'string'],
            [['daily_scrum_id', 'user_id'], 'unique', 'targetAttribute' => ['daily_scrum_id', 'user_id']],
            [['daily_scrum_id'], 'exist', 'skipOnError' => true, 'targetClass' => DailyScrum::className(), 'targetAttribute' => ['daily_scrum_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'daily_scrum_id' => 'Daily Scrum ID',
            'user_id' => 'User ID',
            'time_of_arrival' => 'Time Of Arrival',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyScrum()
    {
        return $this->hasOne(DailyScrum::className(), ['id' => 'daily_scrum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
	 public function setBeenLateValue()
    {
	
		$user=User::find()->where(['id'=>$this->user_id])->one();
		
		$arrived = Participation::find()->where(['id'=>$this->id])->one();
		//$meeting = DailyScrum::find()->select('meeting_start');
		if(strtotime($arrived->time_of_arrival) > strtotime('08:45:00'))
		{
			
			$user->been_late+=1;
			$user->save();
			if($user->update() !== false){
				Yii::$app->session->setFlash('success');
			}else{
				Yii::$app->session->setFlash('error');
			
			}
		}
    }
	 public function getEmployees()
    {
		//return $this->hasMany(User::className(), ["user_id" => "id"])->where(['is_admin' => '0']);
         $employees = (new \yii\db\Query())
		->select(['first_name','last_name','id'])
		->from('user')
		->where(['is_admin' => '0'])
		->limit(10)
		->all();
		
		//funkcija koja sklapa ime i prezime zajedno i vraca niz sa kljucevima koji odgovaraju id-u usera
		foreach($employees as $key=>$value){
			$values[]=$value['first_name']." ".$value['last_name'];
			$keys[]=$value['id'];
		}
		$fullname=array_combine($keys,$values);
		return $fullname;

    }
	
	 public function getScrumDates()
    {
         $scrums = (new \yii\db\Query())
		->select(['date'])
		->from('daily_scrum')
		->all();
		foreach($scrums as $value){
			$dates[]=$value['date'];
		}
		return $dates;

    }

}
