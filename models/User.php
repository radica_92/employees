<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use app\models\Participation;
use app\models\DailyScrum;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property int $is_admin
 * @property int $been_late
 *
 * @property Participation[] $participations
 * @property DailyScrum[] $dailyScrums
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'username', 'password', 'is_admin', 'been_late'], 'required'],
            [['is_admin', 'been_late'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['username'], 'string', 'max' => 45],
            [['password', 'auth_key'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'is_admin' => 'Is Admin',
            'been_late' => 'Been Late',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipations()
    {
        return $this->hasMany(Participation::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyScrums()
    {
        return $this->hasMany(DailyScrum::className(), ['id' => 'daily_scrum_id'])->viaTable('participation', ['user_id' => 'id'])->select(['meeting_start']);
    }
	
	 /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
/* removed
    public static function findIdentityByAccessToken($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
*/
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
	 //Nije implementiran pa je podesen exception u slucaju da neko pokusa pristupiti akciji
    public function generatePasswordResetToken()
    {
        throw new \yii\base\NotSupportedException();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/
 //funckija koja pribavlja sve zaposlene i njihova kasnjenja za slanje podataka site controlleru
	 public function getUserData()
    {
        $user = (new \yii\db\Query())
		->select(['been_late', 'first_name','last_name'])
		->from('user')
		->where(['is_admin' => '0'])
		->limit(10)
		->all();
		return $user;
    }
	 public function getEmployeesForChart()
    {
        $userdata = $this->getUserData();
			foreach($userdata as $value)
		{
			$employees_array[]=$value['first_name'].' '.$value['last_name'];
		}
		return $employees_array;
    }
	
	 public function getBeenLateForChart()
    {
        $userdata = $this->getUserData();
			foreach($userdata as $value)
		{
			$been_late_array[]=$value['been_late'];
		}
		//moramo pretvoriti string clanove niza u int pa koristimo ovu funkciju
		$been_late_array = array_map('intval', $been_late_array);
		return $been_late_array;
    }

}
