<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DailyScrum */

$this->title = Yii::t('app', 'Create Daily Scrum');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Daily Scrums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-scrum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
