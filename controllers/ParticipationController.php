<?php

namespace app\controllers;

use Yii;
use app\models\Participation;
use app\models\DailyScrum;
use app\models\User;
use app\models\ParticipationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\base\Model;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * ParticipationController implements the CRUD actions for Participation model.
 */
class ParticipationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			   'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                   
					 [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete','view'],
                        'roles' => ['admin'],
                    ],
					 [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
			],
			];
    }

    /**
     * Lists all Participation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParticipationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Participation model.
     * @param integer $daily_scrum_id
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($daily_scrum_id, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($daily_scrum_id, $user_id),
        ]);
    }

    /**
     * Creates a new Participation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {	   
			$model = new Participation();
		
			$user=$model->getUser();

			$employee_names=$model->getEmployees();

			$start=$model->getScrumDates();
		

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$model->setBeenLateValue();
				return $this->redirect(['view', 'daily_scrum_id' => $model->daily_scrum_id, 'user_id' => $model->user_id]);
			}

			return $this->render('create', [
				'model' => $model,
				'employee_names' => $employee_names,
				'start' => $start,
			]);
			
    }

    /**
     * Updates an existing Participation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $daily_scrum_id
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($daily_scrum_id, $user_id)
    {		
			$model = $this->findModel($daily_scrum_id, $user_id);
			$start=$model->getScrumDates();
			$employee_names=$model->getEmployees();
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['view', 'daily_scrum_id' => $model->daily_scrum_id, 'user_id' => $model->user_id]);
			}

			return $this->render('update', [
				'model' => $model,
				'start' => $start,
				'employee_names' => $employee_names,
			]);

    }

    /**
     * Deletes an existing Participation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $daily_scrum_id
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($daily_scrum_id, $user_id)
    {
			
		$this->findModel($daily_scrum_id, $user_id)->delete();

		return $this->redirect(['index']);

    }

    /**
     * Finds the Participation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $daily_scrum_id
     * @param integer $user_id
     * @return Participation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($daily_scrum_id, $user_id)
    {
        if (($model = Participation::findOne(['daily_scrum_id' => $daily_scrum_id, 'user_id' => $user_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
